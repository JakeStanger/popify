#!/usr/bin/env bash

set -e

# Space-separated of packages required for the script to run
# These are installed automatically
REQUIRED_PACKAGES="dialog git"

# List of distros that provide their own Gnome Control Center patches
PROVIDES_GCC_PATCHES=("Zorin")

function setup() {
  set -e

  to_install=()

  for package in "$@"; do
    # if [ -z "$(dpkg -l "$package")" ]; then
    to_install+=("$package")
    # fi
  done

  apt install -y "${to_install[@]}"
}

function get_choices() {
  dialog --stdout --checklist "Select Features" 100 100 80 \
    "oled" "Control brightness on OLED displays" "oled" \
    "power" "Utility for managing graphics and power profiles." "power" \
    "hidpi" "Manage HiDPI and LoDPI monitors on X " "hidpi" \
    "firmware" "Check and update firmware from the fwupd and system76-firmware services" "firmware"
}

function install_packages() {
  set -e

  add-apt-repository -y ppa:system76/pop
  apt-get update

  cat >/etc/apt/preferences.d/popos.pref <<-'EOF'
	Package: *
	Pin: release o=LP-PPA-system76-pop
	Pin-Priority: 200
	EOF

  to_install=()

  for choice in "$@"; do
    [[ "$choice" == "oled" ]] && to_install+=(system76-oled)
    [[ "$choice" == "power" ]] && to_install+=(system76-power gnome-shell-extension-system76-power)
    [[ "$choice" == "hidpi" ]] && to_install+=(hidpi-daemon libs76-hidpi-widget)
    [[ "$choice" == "firmware" ]] && to_install+=(firmware-manager)
  done

  apt install -y "${to_install[@]}"
}

function install_control_center() {
  cat >>/etc/apt/preferences.d/popos.pref <<-'EOF'
	Package: gnome-control-center
	Pin: release o=LP-PPA-system76-pop
	Pin-Priority: 600
	EOF

  apt install gnome-control-center
}

function build_control_center() {
  set -e

  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
  echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list >/dev/null
  apt update
  apt install -y docker-ce docker-ce-cli containerd.io

  pushd packages/gnome-control-center

  release="$(lsb_release -si)"

  mkdir -pv patches/.build

  if [ -d "patches/$release" ]
  then
    cp -av "patches/$release/." patches/.build
  else
    touch -v patches/.build/PLACEHOLDER
  fi

  docker build . -t build-gnome-control-center
  docker create -ti --name build-gnome-control-center build-gnome-control-center
  docker cp build-gnome-control-center:/build .

  docker container rm build-gnome-control-center
  docker image rm build-gnome-control-center

  rm -rfv patches/.build

  cp -av build/. /usr

  popd
}

function enable_extensions() {
  for choice in "$@"; do
    if [[ "$choice" == "power" ]]; then
      gnome-extensions enable system76-power@system76
    fi
  done
}

release="$(lsb_release -si)"
requires_gcc_rebuild=$([[ " ${PROVIDES_GCC_PATCHES[*]} " =~ ${release} ]])

if $requires_gcc_rebuild; then
  REQUIRED_PACKAGES+=" apt-transport-https ca-certificates curl gnupg lsb-release"
fi

sudo bash -c "$(declare -f setup); setup $REQUIRED_PACKAGES"

choices=$(get_choices)

if $requires_gcc_rebuild; then
  sudo bash -c "$(declare -f build_control_center); build_control_center || rm -rf gnome-control-center"
else
  sudo bash -c "$(declare -f install_control_center); install_control_center"
fi

sudo bash -c "$(declare -f install_packages); install_packages $choices"
enable_extensions "$choices"

killall -3 gnome-shell
killall -3 gnome-control-center
